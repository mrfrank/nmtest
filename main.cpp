#include <NetworkManager.h>
#include <glib.h> 
#include <stdio.h>

int main() {
    NMClient *client = NULL;
    GError *error = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        printf("Could not connect to NetworkManager: %s, code=%d \n", error->message, error->code);
        return false;
    }
    printf("succeeded \n");
}
